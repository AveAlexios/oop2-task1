import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Student[] students = new Student[10];

        students[0] = new Student("Lehnsherr", "Eric",
                401, new int[]{1, 2, 6, 7, 5});

        students[1] = new Student("Waters", "Roger",
                402, new int[]{9,10,9,8,7});

        students[2] = new Student("Parton", "Dolly",
                403, new int[]{9,9,9,9,9});

        students[3] = new Student("Mitchell", "Joni",
                404, new int[]{10,10,10,10,10});

        students[4] = new Student("Moon", "Keith",
                405, new int[]{2,1,5,7,8});

        students[5] = new Student("Ross", "Diana",
                404, new int[]{10,10,9,9,9});

        students[6] = new Student("Lord", "John",
                403, new int[]{6,4,3,7,8});

        students[7] = new Student("Armstrong", "Louis",
                401, new int[]{1,1,1,1,5});

        students[8]= new Student("Hubbard", "Freddie",
                402, new int[]{10,9,8,9,10});

        students[9] = new Student("Przybytek", "Beata",
                401, new int[]{10,10,9,9,10});


        Student[] result = findingWithOnlyGreatGrades(students, 9);
        System.out.println(Arrays.toString(result));

    }

    private static Student[] findingWithOnlyGreatGrades(Student[] students, int targetGrade){
        Student[] result = new Student[10];
        int count = 0;
        for (Student student : students){
            int[] grades = student.getPerformance();
            if (isGreaterThanNine(grades,targetGrade)){
                result[count++] = student;
            }
        }
        Student[] destination = new Student[count];
        System.arraycopy(result, 0, destination, 0, count);
        return destination;
    }

    private static boolean isGreaterThanNine(int[] grades, int target){
        for (int grade : grades){
            if (grade < target){
                return false;
            }
        }
        return true;
    }
}