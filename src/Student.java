public class Student {
    private String surname;
    private String name;
    private int groupNumber;
    private int[] performance;

    public Student(String surname, String name, int groupNumber, int[] performance) {
        this.surname = surname;
        this.name = name;
        this.groupNumber = groupNumber;
        this.performance = performance;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int[] getPerformance() {
        return performance;
    }

    public void setPerformance(int[] performance) {
        this.performance = performance;
    }

    public String toString(){
        return this.name + " " + this.surname + " " +this.groupNumber + " ";
    }
}
